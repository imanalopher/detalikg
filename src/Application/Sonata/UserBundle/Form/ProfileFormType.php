<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\UserBundle\Form;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;

class ProfileFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // add your custom field
        $builder
            ->add('phone', null, array(
                'label' => 'Тел:'
            ))
            ->add('biography', null, array(
                'label' => 'Адрес:'
            ))
            ->add('description', null, array(
                'label' => 'Описание'
            ))
            ->add('firstname', null, array(
                'label' => 'Имя пользователя'
            ))
            ->add('gender', 'choice', array(
                'choices' => array('m' => 'Мужской', 'f' => 'Женский пол'),
                'expanded' => true)
            );
    }

    public function getName()
    {
        return 'acme_user_profile';
    }
}
