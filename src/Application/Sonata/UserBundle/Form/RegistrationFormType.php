<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.09.2016
 * Time: 14:09
 */

namespace Application\Sonata\UserBundle\Form;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\FormBuilder;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // add your custom field
        $builder->add('captcha', CaptchaType::class, array(
            'label' => false,
            'width' => 121,
            'height' => 60,
            'length' => 4,
            'reload' => true,
            'as_url' => true,
            'invalid_message' => "Вы ввели неправильный код с картинки (CAPTCHA)",
            'label_attr' => array('class' => 'required d_inline_b m_bottom_5')
        ))
            ->add('biography', null, array(
                'required' => "required",
                'label_attr' => array('class' => 'required d_inline_b m_bottom_5'),
                'attr' => array('placeholder' => 'адрес', 'class' => 'form-control')
            ))
            ->add('firstname', null, array(
                'required' => "required",
                'label' => 'Имя',
                'label_attr' => array('class' => 'col-sm-3 control-label'),
                'attr' => array('placeholder' => 'Иван', 'class' => 'form-control')
            ))
            ->add('phone', null, array(
                'required' => "required",
                'invalid_message' => "Вы ввели неправильный тел. номер",
                'label_attr' => array('class' => 'required d_inline_b m_bottom_5')
        ));
    }

    public function getName()
    {
        return 'acme_user_registration';
    }
}