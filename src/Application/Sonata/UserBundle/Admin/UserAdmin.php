<?php
namespace Application\Sonata\UserBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\DateTime;

class UserAdmin extends BaseUserAdmin
{
    /**
     * Default values to the datagrid.
     *
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_by'    => 'id',
        '_sort_order' => 'DESC',
    );
    protected $translationDomain = 'SonataPageBundle';

    protected function configureListFields(ListMapper $listMapper)
    {
//        parent::configureListFields($listMapper);
        $listMapper
            ->remove('groups')
            ->addIdentifier('username')
            ->add('email')
            ->add('firstname', null, array('editable' => true))
            ->add('phone')
            ->add('isStore', null, array('editable' => true))
            ->add('hot', null, array('editable' => true))
            ->add('isService', null, array('editable' => true))
            ->add('razbor', 'boolean', array('editable' => true, 'label' => 'Авторазбор'))
            ->add('enabled', null, array('editable' => true))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper
            ->tab('User')
            ->with('Profile', array('class' => 'col-md-6'))->end()
            ->with('General', array('class' => 'col-md-6'))->end()
            ->end()
            ->tab('Security')
            ->with('Status', array('class' => 'col-md-4'))->end()
            ->with('Roles', array('class' => 'col-md-8'))->end()
            ->end()
        ;

        $now = new \DateTime();

        $formMapper
            ->tab('User')
            ->with('General')
            ->add('username')
            ->add('email')
            ->add('plainPassword', 'text', array(
                'required' => (!$this->getSubject() || is_null($this->getSubject()->getId())),
            ))
            ->add('photo', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context' => 'default',
                'required'=>false
            ))
            ->end()
            ->with('Profile')
            ->add('firstname', null, array('required' => false))
            ->add('lastname', null, array('required' => false))
            ->add('biography', 'text', array('required' => false))
            ->add('phone', null, array('required' => false))
            ->add('description', null, array('required' => true))
            ->end()
            ->end()
            ->tab('Security')
            ->with('Status')
            ->add('locked', null, array('required' => false))
            ->add('expired', null, array('required' => false))
            ->add('enabled', null, array('required' => false))
            ->add('credentialsExpired', null, array('required' => false))
            ->end()
            ->with('Roles')
            ->add('realRoles', 'sonata_security_roles', array(
                'label' => 'form.label_roles',
                'expanded' => true,
                'multiple' => true,
                'required' => false,
            ))
            ->end()
            ->end()
        ;
    }
}