<?php

namespace Application\Sonata\UserBundle\Entity;

use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Regex(
     *     pattern     = "/^[a-z0-9]+$/i",
     *     htmlPattern = "^[a-zA-Z0-9]+$",
     *     message="Логин без пробелов, только английские буквы и можно цифры"
     * )
     */
    public $username;

    /**
     * @var string $image
     */
    protected $photo;

    /**
     * @var bool
     *
     * @ORM\Column(name="hot", type="boolean", nullable=true, options={"default" : false})
     */
    private $hot;

    /**
     * @var bool
     *
     * @ORM\Column(name="isService", type="boolean", nullable=false, options={"default" : false})
     */
    private $isService = false;

    /**
     * @var int
     *
     * @ORM\Column(name="uploadLimit", type="integer", nullable=true, options={"default" : 20})
     */
    private $limit;

    /**
     * @var bool
     *
     * @ORM\Column(name="isStore", type="boolean", nullable=false, options={"default" : false})
     */
    private $isStore = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="razbor", type="boolean", nullable=false, options={"default" : false})
     */
    private $razbor = false;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description = '';


    public function __construct()
    {
        parent::__construct();
        $this->setLimit(20);
        $this->setHot(1);
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }
    /**
     * @param string $image
     */
    public function setPhoto($image)
    {
        $this->photo = $image;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return boolean
     */
    public function isHot()
    {
        return $this->hot;
    }

    /**
     * @param boolean $hot
     */
    public function setHot($hot)
    {
        $this->hot = $hot;
    }

    /**
     * @return bool
     */
    public function isIsStore()
    {
        return $this->isStore;
    }

    /**
     * @param bool $isStore
     */
    public function setIsStore($isStore)
    {
        $this->isStore = $isStore;
    }

    /**
     * @return mixed
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param mixed $imageId
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;
    }

    /**
     * @return bool
     */
    public function isIsService()
    {
        return $this->isService;
    }

    /**
     * @param bool $isService
     */
    public function setIsService($isService)
    {
        $this->isService = $isService;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function getRazbor()
    {
        return $this->razbor;
    }

    /**
     * @param bool $razbor
     */
    public function setRazbor($razbor)
    {
        $this->razbor = $razbor;
    }
}