<?php

namespace Shop\BasketBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BasketController extends Controller
{
    /**
     * @Route("/basket/add/", requirements={"id" = "\d+"})
     */
    public function addToBasketAction(Request $request)
    {
        $goodsId = $request->get('objectId');
        $basket = $this->get('shop_basket.session')->getBasketSession();
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->find($goodsId);
        if(!$goods)
            throw new EntityNotFoundException();

        if(isset($basket[$goods->getId()]))
        {
            $basket[$goods->getId()] = array(
                'id' => $goods->getId(),
                'count' => $basket[$goods->getId()]['count'] + 1,
                'price' => $goods->getPrice()
            );
        }
        else
        {
            $basket[$goods->getId()] = array(
                'id' => $goods->getId(),
                'count' => 1,
                'price' => $goods->getPrice()
            );
        }

        $totalSum = 0;
        $count = 0;
        foreach ($basket as $item) {
            $count += $item['count'];
            $totalSum += $item['price'] * $item['count'];
        }

        $this->get('shop_basket.session')->setBasketSession($basket);

        return new JsonResponse(array('count' => $count, 'totalSum' => $totalSum));
    }

    public function basketAction()
    {
        $basket = $this->get('shop_basket.session')->getBasketSession();
        $priceSum = 0;
        $count = 0;
        if (is_array($basket) || is_object($basket))
        {
            foreach ($basket as $item) {
                $priceSum += $item['price'] * $item['count'];
                $count += $item['count'];
            }
        }
        return $this->render('@Basket/Basket/basket.html.twig', array('sum' => $priceSum, 'count'=>$count));
    }
}
