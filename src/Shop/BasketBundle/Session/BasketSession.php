<?php

namespace Shop\BasketBundle\Session;

use Symfony\Component\HttpFoundation\Session\Session;

class BasketSession
{
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function setBasketSession($basket)
    {
        $this->session->set('basket', $basket);
    }

    public function getBasketSession()
    {
        $basket = $this->session->get('basket');
        return $basket;
    }

    public function getIdsBasketSession()
    {
        $baskets = $this->session->get('basket');
        $ids = array();
        foreach ($baskets as $basket)
        {
            $ids[] = $basket['id'];
        }
        return $ids;
    }

    public function removeBasket()
    {
        $this->session->clear();
    }
}