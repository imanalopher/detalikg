<?php

namespace Shop\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shop\NewsBundle\Entity\News;

/**
 * News controller.
 *
 * @Route("/news")
 */
class NewsController extends Controller
{
    /**
     * Lists all News entities.
     *
     * @Route("/", name="news_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $news = $em->getRepository('ShopNewsBundle:News')->findBy(array(), array('date' => 'DESC'));

        return $this->render('ShopNewsBundle:News:index.html.twig', array(
            'news' => $news,
        ));
    }

    /**
     * Finds and displays a News entity.
     *
     * @Route("/{id}", name="news_show")
     * @Method("GET")
     */
    public function showAction(News $news)
    {
        $em = $this->getDoctrine()->getManager();
        $view = $news->getView();
        $news->setView($view + 2);
        $em->persist($news);
        $em->flush();
        return $this->render('ShopNewsBundle:News:show.html.twig', array(
            'news' => $news
        ));
    }
}
