<?php

namespace Shop\NewsBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Shop\CatalogBundle\Entity\Goods;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class NewsAdmin extends AbstractAdmin
{

    protected function configureShowField(ShowMapper $showmapper)
    {
        $showmapper
            ->add('id', null, array('label' => 'ID'))
            ->add('title', null, array('label' => 'Название'))
            ->add('description', null, array('label' => 'Полное описание'))
            ->add('date', 'datetime', array('label' => 'Последнее обновление'));

    }

    protected function configureFormFields(FormMapper $formmapper)
    {
        $formmapper
            ->with('General')
            ->add('title', null, array('label' => 'Название'))
            ->add('url', null, array(
                    'label' => 'Youtube ссылка',
                    'required' => false
                )
            )
            ->add('description', CKEditorType::class,
                array('label' => 'Обзор', 'config' => array('width' => '1300px', 'resize_enabled' => true, 'resize_minHeight' => '745px', 'resize_minWidth' => '700px', 'resize_maxWidth' => '700px'), 'config_name' => 'my_config')
            )
            ->end();
    }

    protected function configureListFields(ListMapper $listmapper)
    {
        $listmapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->add('title', null, array('label' => 'Название'))
            ->add('date', 'datetime', array('label' => 'Последнее обновление', 'editable' => true))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array('label' => 'Название'));
    }

}