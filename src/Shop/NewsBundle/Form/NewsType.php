<?php

namespace Shop\NewsBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Название',
                'attr' => array('class' => 'form-control')
            ))
            ->add('description', CKEditorType::class, array(
                    'label' => 'Обзор',
                    'config' => array(
                        'width' => '800px',
                        'resize_enabled' => true
                    ),
                    'config_name' => 'my_config')
            )
            ->add('url')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shop\NewsBundle\Entity\News'
        ));
    }
}
