<?php

namespace Shop\CommentBundle\Form;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class FeedbackType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Имя',
                'attr' => array('class' => 'form-control')
            ))
            ->add('captcha', CaptchaType::class, array(
                'label' => false,
                'width' => 121,
                'height' => 60,
                'length' => 4,
                'reload' => true,
                'as_url' => true,
                'invalid_message' => "Вы ввели неправильный код с картинки (CAPTCHA)",
                'label_attr' => array('class' => 'form-control')
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Телефон',
                'required' => true,
                'constraints' =>[
                    new NotBlank([
                        'message'=>'Телефон формат +996 (707) xx - xx - xx'
                    ])
                ],
                'attr' => array('class' => 'form-control')
            ))
            ->add('comment', 'textarea', array(
                'label' => 'Комментарий',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Комментарий')
            ))
            ->add('submit', 'submit', array(
                'label' => 'Отправить',
                'attr' => array('class' => 'btn btn-primary')
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shop\CommentBundle\Entity\Feedback'
        ));
    }
}
