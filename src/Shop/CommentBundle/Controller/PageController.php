<?php

namespace Shop\CommentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Shop\CommentBundle\Entity\Feedback;
use Shop\CommentBundle\Form\FeedbackType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller
{

    /**
     * @Route("/about.html", name="aboutPage")
     * @Method({"GET"})
     * @return Response
     */
    public function aboutAction()
    {
        $feedback = new Feedback();
        $form = $this->createForm(new FeedbackType(), $feedback);
        $feedbacks = $this->getDoctrine()->getRepository('CommentBundle:Feedback')->findBy([], array('datetime' => 'DESC'), 10);
        return $this->render('CommentBundle:Page:about.html.twig', ['form' => $form->createView(), 'feedbacks' => $feedbacks]);
    }

    /**
     * @Route("/post/feedback", name="postFeedback")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function postFeedbackAction(Request $request)
    {
        $feedback = new Feedback();
        $form = $this->createForm(new FeedbackType(), $feedback);
        $form->handleRequest($request);
        if($form->isValid())
        {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($feedback);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('aboutPage'));
    }
}
