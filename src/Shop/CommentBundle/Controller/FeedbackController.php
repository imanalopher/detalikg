<?php

namespace Shop\CommentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Shop\CommentBundle\Entity\Feedback;
use Shop\CommentBundle\Form\FeedbackType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FeedbackController extends Controller
{
    /**
     * @Route("/feedback", name="feedback")
     * @return Response
     */
    public function feedbackAction(Request $request)
    {
        $feedback = new Feedback();
        $form = $this->createForm(new FeedbackType(), $feedback);
        if($request->isMethod('POST'))
        {
            $em = $this->getDoctrine()->getManager();
            $form->handleRequest($request);
            if ($form->isValid()) {
                $feedback = $form->getData();
                $em->persist($feedback);
                $em->flush();
                return $this->redirect($this->generateUrl('feedback'));
            }
        }
        $dql = 'SELECT f FROM CommentBundle:Feedback f';
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            20/*limit per page*/
        );
        return $this->render('CommentBundle:Feedback:feedback.html.twig', ['form' => $form->createView(), 'feedbacks' => $pagination ]);
    }
}