<?php

namespace Shop\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CollectionController extends Controller
{
    /**
     * @Route("/collections", name="collections")
     * @return Response
     */
    public function indexAction()
    {
        $collections = $this->getDoctrine()->getRepository('CatalogBundle:Collection')->getAllActive();
        if(!$collections)
            throw new NotFoundHttpException("Page not found");
        return $this->render('@Catalog/Collection/index.html.twig', ['collections' => $collections]);
    }

    public function listAction()
    {
        $collections = $this->getDoctrine()->getRepository('CatalogBundle:Collection')->getActiveNotNull();
        return $this->render('@Catalog/Collection/list.html.twig', ['collections' => $collections]);
    }
}
