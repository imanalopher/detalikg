<?php

namespace Shop\CatalogBundle\Controller;

use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use Application\Sonata\MediaBundle\Entity\Media;
use Shop\CatalogBundle\Entity\Goods;
use Shop\CatalogBundle\Form\GoodsType;
use Sonata\MediaBundle\Model\MediaInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    /**
     * @Route("/", name="homePage")
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        return $this->render('CatalogBundle:Default:index.html.twig');
    }
    /**
     * @Route(name="mobileMenu")
     */
    public function mobileMenuAction()
    {
        $categories = $this->getDoctrine()->getManager()->getRepository('CatalogBundle:Category')->findBy(array('active' => 1, 'parent' => null));
        return $this->render('CatalogBundle:Default:mobileMenu.html.twig', ['categories' => $categories]);
    }

    /**
     * @Route("/search/", name="searchPage", options = { "expose" = true })
     * @param Request $request
     * @return JsonResponse
     */
    public function searchAction(Request $request)
    {
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findByKeyWord($request->get('keyword'));
        $arr = array();
        /** @var Goods $product */
        foreach ($goods as $product) //goods_small
        {
            $galleryHasMedia = $product->getImagePath()->getGalleryHasMedias() ? $product->getImagePath()->getGalleryHasMedias() : null;
            $media = is_null($galleryHasMedia)? '' : $galleryHasMedia[0]->getMedia();
            $path = $media instanceof MediaInterface ? $this->container->get('sonata.media.twig.extension')->path($media, 'goods_small') : '';

            $url = $url = $this->generateUrl('goodsGetInfo', array('id' => $product->getId()));
            $arr[] = array('name' => $product->getName(), 'href' => $url, 'image' => $path);
        }
        return new JsonResponse($arr);
    }


    /**
     * @Route("/uploadProduct", name="uploadProduct")
     */
    public function uploadProductAction(Request $request)
    {
        if(!$this->getUser()) {
            return $this->render('@Catalog/Upload/uploadLimit.html.twig');
        }
        $goods = new Goods();
        $form = $this->createForm(new GoodsType(), $goods);
        if($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $file = $request->files->get('goods')['imagePath'];

                if(count($file) > 1) {
                    /** @var Goods $goods */
                    $goods = $form->getData();
                    $goods->setLastUpdate(new \DateTime());
                    $mediaManager = $this->container->get('sonata.media.manager.media');

                    $gallery = new Gallery();
                    $date = new \DateTime();
                    $gallery->setName($this->getUser()->getId()."-".$date->format('Y-m-d-H:i:s'));
                    $gallery->setEnabled(1);
                    $gallery->setDefaultFormat('default_goods_small');
                    $gallery->setContext('default');

                    foreach ($file as $item) {
                        $media = new Media();
                        $media->setBinaryContent($item);
                        $media->setContext('default');
                        $media->setProviderName('sonata.media.provider.image');
                        $mediaManager->save($media);

                        $galleryHasMedia = new GalleryHasMedia();
                        $galleryHasMedia->setMedia($media);
                        $galleryHasMedia->setEnabled(true);

                        $gallery->addGalleryHasMedias($galleryHasMedia);
                        $em->persist($galleryHasMedia);
                        $em->persist($media);
                    }
                    $goods->setUser($this->getUser());
                    $goods->setImagePath($gallery);
                    $em->persist($goods);
                    $em->flush();
                }
                else{
                    /** @var Goods $goods */
                    $goods = $form->getData();

                    $goods->setLastUpdate(new \DateTime());
                    $mediaManager = $this->container->get('sonata.media.manager.media');

                    $media = new Media();
                    $media->setBinaryContent($file);
                    $media->setContext('default');
                    $media->setProviderName('sonata.media.provider.image');
                    $mediaManager->save($media);

                    $galleryHasMedia = new GalleryHasMedia();
                    $galleryHasMedia->setMedia($media);
                    $galleryHasMedia->setEnabled(true);

                    $gallery = new Gallery();
                    $date = new \DateTime();
                    $gallery->setName($this->getUser()->getId()."-".$date->format('Y-m-d-H:i:s'));
                    $gallery->setEnabled(1);
                    $gallery->setDefaultFormat('default_goods_small');
                    $gallery->setContext('default');
                    $gallery->addGalleryHasMedias($galleryHasMedia);

                    $goods->setUser($this->getUser());
                    $goods->setImagePath($gallery);


                    $em->persist($galleryHasMedia);
                    $em->persist($media);
                    $em->persist($goods);
                    $em->flush();

                }
                return $this->redirect($this->generateUrl('goodsGetInfo', array('id' => $goods->getId())));
            }
            else {
                dump($form->getErrors(true));
                die('not falid');
            }
        }
        return $this->render('CatalogBundle:Upload:uploadProduct.html.twig', array('form' => $form->createView()));
    }
}
