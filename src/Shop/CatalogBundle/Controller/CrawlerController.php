<?php

namespace Shop\CatalogBundle\Controller;

use Application\Sonata\UserBundle\Entity\User;
use DOMDocument;
use DOMXPath;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Application\Sonata\MediaBundle\Entity\Gallery;
use Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use Application\Sonata\MediaBundle\Entity\Media;
use Shop\CatalogBundle\Entity\Goods;

class CrawlerController extends Controller
{
    /**
     * @Route("/crawler_lalafo", name="crawler_lalafo")
     */
    public function indexAction()
    {
        $response = $this->request("https://lalafo.kg/kyrgyzstan/zapchasti-i-aksessuary/zapchasti");

        $html_dom = new DOMDocument("1.0", "UTF-8");

        @$html_dom->loadHTML(mb_convert_encoding($response, 'HTML-ENTITIES', 'UTF-8'));
        $x_path = new DOMXPath($html_dom);

        $count = $x_path->query("//div[@event-action='tap']")->length;
        $em = $this->getDoctrine()->getManager();
        $admin = $em->getRepository('ApplicationSonataUserBundle:User')->findOneBy(array('username' => 'admin'));
        if(!$admin instanceof User)
            $admin = $em->getRepository('ApplicationSonataUserBundle:User')->findOneBy(array('username' => 'detali.kg'));
        if(!$admin instanceof User)
            $admin = $em->getRepository('ApplicationSonataUserBundle:User')->findOneBy(array('email' => 'detalikg28@gmail.com'));

        if($count > 20)
            $count = 20;

        for ($i = 0; $i < $count; $i++)
        {
            if(strpos($x_path->query("//div[@event-action='tap']")->item($i)->getAttribute('class'), 'vip-ad') === false) {
                $img = null;
                $phone = "";
                $description = "";
                $title = "";
                $price = 0;

                foreach ($x_path->query("//div[@event-action='tap']")->item($i)->getElementsByTagName('a') as $item)
                {
                    if($item->getAttribute('class') == 'name')
                    {
                        if(strlen($item->getAttribute('href')) > 0) {
                            $response2 = $this->requestMobile("https://m.lalafo.kg" . $item->getAttribute('href'));
                            $html_dom2 = new DOMDocument("1.0", "UTF-8");

                            @$html_dom2->loadHTML(mb_convert_encoding($response2, 'HTML-ENTITIES', 'UTF-8'));
                            $x_path2 = new DOMXPath($html_dom2);
                            $tel = $x_path2->query("//a[@id='phoneCall']")->item(0)->getAttribute('href');
                            $description = trim($x_path2->query("//p[@class='description']")->item(0)->nodeValue);
                            $phone = str_replace("tel:", "", $tel);

                        }
                    }
                }

                foreach ($x_path->query("//div[@event-action='tap']")->item($i)->getElementsByTagName('img') as $item)
                {
                    if(is_null($item->getAttribute('src')))
                        $img = $item->getAttribute('src');
                    else
                        $img = str_replace('/api/', '/original/', $item->getAttribute('src'));
                }

                foreach ($x_path->query("//div[@event-action='tap']")->item($i)->getElementsByTagName('a') as $item)
                {
                    if($item->getAttribute('class') !== 'adv-to-fav listing' && $item->getAttribute('class') !== 'image-holder')
                        $title =  $item->nodeValue;
                }

                foreach ($x_path->query("//div[@event-action='tap']")->item($i)->getElementsByTagName('div') as $item)
                {
                    if(strpos(str_replace(" ", "", $item->nodeValue), 'Договорная') !== false){
                        $price = 0;
                    }
                    elseif($item->getAttribute('class') == 'price') {
                        $second = preg_replace('/[^0-9]/','', $item->nodeValue);

                        $price = intval($second);

                    }
                }

                $image_name = explode("/", $img);
                $save_directory = $this->get('kernel')->getRootDir().'/../web/public/parse/';

                if(is_writable($save_directory)) {
                    file_put_contents($save_directory . $image_name[count($image_name) - 1], file_get_contents($img));
                } else {
                    exit("can't write");
                }
                $uploadFile = new UploadedFile(
                    $save_directory . $image_name[count($image_name) - 1],
                    $image_name[count($image_name) - 1]
                );

                $good = new Goods();

                $good->setName($title);
                $good->setLastUpdate(new \DateTime());
                $category = $em->getRepository('CatalogBundle:Category')->find(1);
                $good->setCategory($category);
                $good->setPrice($price);
                $good->setPhone($phone);
                $shortDesc = substr($description, 0, 249);
                $good->setShortDescription((string)$shortDesc);
                $good->setFullDescription($description);
                $mediaManager = $this->container->get('sonata.media.manager.media');

                $media = new Media();
                $media->setBinaryContent($uploadFile);
                $media->setContext('default');
                $media->setProviderName('sonata.media.provider.image');
                $mediaManager->save($media);

                $galleryHasMedia = new GalleryHasMedia();
                $galleryHasMedia->setMedia($media);
                $galleryHasMedia->setEnabled(true);

                $gallery = new Gallery();
                $date = new \DateTime();
                $gallery->setName($admin->getId()."-".$date->format('Y-m-d-H:i:s'));
                $gallery->setEnabled(1);
                $gallery->setDefaultFormat('default_goods_small');
                $gallery->setContext('default');
                $gallery->addGalleryHasMedias($galleryHasMedia);

                $good->setUser($admin);
                $good->setImagePath($gallery);


                $em->persist($galleryHasMedia);
                $em->persist($media);
                $em->persist($good);

            }
        }
        $em->flush();
        return $this->redirectToRoute("homePage");
    }

    /**
     * @Route("/disc/crawler", name="disc_crawler", methods={"GET"})
     */
    public function discAction()
    {
        $response = $this->request("https://lalafo.kg/kyrgyzstan/zapchasti-i-aksessuary/shiny-i-diski");

        $html_dom = new DOMDocument("1.0", "UTF-8");

        @$html_dom->loadHTML(mb_convert_encoding($response, 'HTML-ENTITIES', 'UTF-8'));
        $x_path = new DOMXPath($html_dom);

        $count = $x_path->query("//div[@event-action='tap']")->length;
        $em = $this->getDoctrine()->getManager();
        $admin = $em->getRepository('ApplicationSonataUserBundle:User')->findOneBy(array('username' => 'detali.kg'));
        if(!$admin instanceof User)
            $admin = $em->getRepository('ApplicationSonataUserBundle:User')->findOneBy(array('username' => 'admin'));
        if(!$admin instanceof User)
            $admin = $em->getRepository('ApplicationSonataUserBundle:User')->findOneBy(array('email' => 'detalikg28@gmail.com'));

        if($count > 20)
            $count = 20;

        for ($i = 0; $i < $count; $i++)
        {
            if(strpos($x_path->query("//div[@event-action='tap']")->item($i)->getAttribute('class'), 'vip-ad') === false) {
                $img = null;
                $phone = "";
                $description = "";
                $title = "";
                $price = 0;

                foreach ($x_path->query("//div[@event-action='tap']")->item($i)->getElementsByTagName('a') as $item)
                {
                    if($item->getAttribute('class') == 'name')
                    {
                        if(strlen($item->getAttribute('href')) > 0) {
                            $response2 = $this->requestMobile("https://m.lalafo.kg" . $item->getAttribute('href'));
                            $html_dom2 = new DOMDocument("1.0", "UTF-8");

                            @$html_dom2->loadHTML(mb_convert_encoding($response2, 'HTML-ENTITIES', 'UTF-8'));
                            $x_path2 = new DOMXPath($html_dom2);
                            $tel = $x_path2->query("//a[@id='phoneCall']")->item(0)->getAttribute('href');
                            $description = trim($x_path2->query("//p[@class='description']")->item(0)->nodeValue);
                            $phone = str_replace("tel:", "", $tel);

                        }
                    }
                }

                foreach ($x_path->query("//div[@event-action='tap']")->item($i)->getElementsByTagName('img') as $item)
                {
                    if(is_null($item->getAttribute('src')))
                        $img = $item->getAttribute('src');
                    else
                        $img = str_replace('/api/', '/original/', $item->getAttribute('src'));
                }

                foreach ($x_path->query("//div[@event-action='tap']")->item($i)->getElementsByTagName('a') as $item)
                {
                    if($item->getAttribute('class') !== 'adv-to-fav listing' && $item->getAttribute('class') !== 'image-holder')
                        $title =  $item->nodeValue;
                }

                foreach ($x_path->query("//div[@event-action='tap']")->item($i)->getElementsByTagName('div') as $item)
                {
                    if(strpos(str_replace(" ", "", $item->nodeValue), 'Договорная') !== false){
                        $price = 0;
                    }
                    elseif($item->getAttribute('class') == 'price') {
                        $second = preg_replace('/[^0-9]/','', $item->nodeValue);

                        $price = intval($second);

                    }
                }

                $image_name = explode("/", $img);
                $save_directory = $this->get('kernel')->getRootDir().'/../web/public/parse/';

                if(is_writable($save_directory)) {
                    file_put_contents($save_directory . $image_name[count($image_name) - 1], file_get_contents($img));
                } else {
                    exit("can't write");
                }
                $uploadFile = new UploadedFile(
                    $save_directory . $image_name[count($image_name) - 1],
                    $image_name[count($image_name) - 1]
                );

                $good = new Goods();

                $good->setName($title);
                $good->setLastUpdate(new \DateTime());
                if(strpos(mb_strtolower($title), ' шин ') !== false || strpos(mb_strtolower($title), ' шины ') !== false || strpos(mb_strtolower($title), ' шина ') !== false || strpos(mb_strtolower($title), 'резин') !== false)
                    $category = $em->getRepository('CatalogBundle:Category')->find(7);
                else
                    $category = $em->getRepository('CatalogBundle:Category')->find(4);

                $good->setCategory($category);
                $good->setPrice($price);
                $good->setPhone($phone);
                $shortDesc = substr($description, 0, 249);
                $good->setShortDescription((string)$shortDesc);
                $good->setFullDescription($description);
                $mediaManager = $this->container->get('sonata.media.manager.media');

                $media = new Media();
                $media->setBinaryContent($uploadFile);
                $media->setContext('default');
                $media->setProviderName('sonata.media.provider.image');
                $mediaManager->save($media);

                $galleryHasMedia = new GalleryHasMedia();
                $galleryHasMedia->setMedia($media);
                $galleryHasMedia->setEnabled(true);

                $gallery = new Gallery();
                $date = new \DateTime();
                $gallery->setName($admin->getId()."-".$date->format('Y-m-d-H:i:s'));
                $gallery->setEnabled(1);
                $gallery->setDefaultFormat('default_goods_small');
                $gallery->setContext('default');
                $gallery->addGalleryHasMedias($galleryHasMedia);

                $good->setUser($admin);
                $good->setImagePath($gallery);


                $em->persist($galleryHasMedia);
                $em->persist($media);
                $em->persist($good);
            }
        }
        $em->flush();
        return $this->redirectToRoute("homePage");
    }

    public function request($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url ); //Url together with parameters
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 10); //Timeout after 7 seconds
        curl_setopt($ch, CURLOPT_ENCODING ,"");
        curl_setopt($ch, CURLOPT_USERAGENT,  'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function requestMobile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url ); //Url together with parameters
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 10); //Timeout after 7 seconds
        curl_setopt($ch, CURLOPT_ENCODING ,"");
        curl_setopt($ch, CURLOPT_USERAGENT,  'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Mobile Safari/537.36');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}