<?php

namespace Shop\CatalogBundle\Controller;

use Shop\CatalogBundle\Entity\Goods;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends Controller
{
    /**
     * @Route("/", name="homePage")
     */
    public function indexAction()
    {
        $category = $this->getDoctrine()->getRepository('CatalogBundle:Category')->findBy(array('parent' => null));

        return $this->render('@Catalog/Category/index.html.twig', array('categories' => $category));
    }

    /**
     * @Route("/catalog/{id}", options={"expose" = true }, name="getCategory", requirements={"id" = "\d+"})
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function getCategoryAction($id, Request $request)
    {
        $maxValue = $request->get('maxValue')?$request->get('maxValue'):0;
        $minVal = $request->get('minValue')?$request->get('minValue'):0;
        $brandIds = array();
        $category = $this->getDoctrine()->getRepository('CatalogBundle:Category')->find($id);
        if(!$category)
            throw new NotFoundHttpException();
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->getIds();
        $collection = $this->getDoctrine()->getRepository('CatalogBundle:Collection')->findBy(array('category' => $id), array(), 1);
        $maxPrice = 0;
        /** @var Goods $product */
        foreach ($goods as $product) {
            $brandIds[] = $product->getBrand()->getId();
            if($product->getPrice() > $maxPrice)
                $maxPrice = $product->getPrice();
        }
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seoPage->getTitle()." - ".$category->getCname())
            ->addMeta('property', 'og:title', $category->getCname())
            ->addMeta('property', 'og:type', 'website')
            ->addMeta('property', 'og:url', $request->getUri())
        ;
        $manufacturers = $this->getDoctrine()->getRepository('CatalogBundle:Manufacture')->findBy(array('active' => 1));
        return $this->render('CatalogBundle:Category:getCategory.html.twig',
            ['id' => $id,'minValue' => $minVal, 'maxValue' => $maxValue, 'maxPrice' => $maxPrice, 'brandIds' => $brandIds, 'collection' => $collection, 'category' => $category, 'page' => $request->query->getInt('page', 1), 'manufacturers' => $manufacturers, 'request' => $request]
        );
    }
}
