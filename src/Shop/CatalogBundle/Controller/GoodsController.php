<?php

namespace Shop\CatalogBundle\Controller;

use Shop\CatalogBundle\Entity\Comment;
use Shop\CatalogBundle\Entity\Goods;
use Shop\CatalogBundle\Form\CommentType;
use Shop\CatalogBundle\Form\GoodsType;
use Shop\CatalogBundle\Form\GoodsTypeTwo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class GoodsController extends Controller
{
    /**
     * @Route("/index", name="goodsIndex")
     */
    public function indexAction()
    {
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findBy(array('active' => true), array('id' => 'DESC'), 12);
        return $this->render('CatalogBundle:Goods:index.html.twig', array('goods' => $goods));
    }

    /**
     * @Route("/catalog/product/{id}", name="goodsGetInfo")
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function getInfoAction($id, Request $request)
    {
        $product = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->find($id);
        if (!$product || !$product->getActive())
            throw $this->createNotFoundException('Page not found 404');
        $seoPage = $this->container->get('sonata.seo.page');
        $comment = new Comment();
        $form = $this->createForm(new CommentType(), $comment);
        $em = $this->getDoctrine()->getManager();
        if($request->isMethod('POST'))
        {
            $form->submit($request);
            if($form->isValid())
            {
                $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->find($id);

                $comment->setProduct($goods);
                $em->persist($comment);
                $em->flush();
                $comments = $this->getDoctrine()->getRepository('CatalogBundle:Comment')->getAVGRating($id);
                if($comments[1])
                    $goods->setRating($comments[1]);
                else
                    $goods->setRating($request->request->get('comment')['rating']);
                $em->persist($goods);
            }
        }
        $seoPage
            ->setTitle($product->getName() . ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('property', 'og:title', $product->getName())
            ->addMeta('property', 'og:type', 'product')
            ->addMeta('property', 'og:url', $request->getUri())
            ->addMeta('name', 'description', $product->getName() . ' ' . 'в Бишкеке Detali.kg')
        ;
        $reviews = $product->getReviews();
        $product->setReviews($reviews + 1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();
        $comments = $this->getDoctrine()->getRepository('CatalogBundle:Comment')->findBy(array('product' => $id), array('id' => 'desc'));
        return $this->render('CatalogBundle:Goods:productInfo.html.twig', array('product' => $product, 'form' => $form->createView(), 'comments' => $comments));
    }

    /**
     * @Route("/product/post_comment/{id}", name="postComment", methods={"POST"}, requirements  = { "id" = "\d+" })
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function getCommentAction($id, Request $request)
    {
        $comment = new Comment();
        $form = $this->createForm(new CommentType(), $comment);
        $form->handleRequest($request);
        if($form->isValid())
        {
            $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->find($id);
            $em = $this->getDoctrine()->getEntityManager();
            $comment->setProduct($goods);
            $em->persist($comment);
            $em->flush();
            $comments = $this->getDoctrine()->getRepository('CatalogBundle:Comment')->getAVGRating($id);
            $goods->setRating($comments[1]);
            $em->persist($goods);
            $em->flush();
        }
        else{
            $form = $this->createForm(new CommentType(), $comment);
            $product = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->find($id);
            return $this->render('CatalogBundle:Goods:productInfo.html.twig', array('product' => $product, 'form' => $form->createView()));
        }
    }

    /**
     * @Route("/popular/product", name="popularProducts", methods={"GET"})
     * @return Response
     */
    public function popularProductAction(Request $request)
    {

        $em    = $this->getDoctrine()->getManager();
        $dql   = "SELECT g FROM CatalogBundle:Goods g ORDER BY g.reviews DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            12/*limit per page*/
        );
        return $this->render('@Catalog/Goods/popularProduct.html.twig', ['goods' => $pagination]);
    }

    /**
     * @Route("/products/sale", name="saleProducts", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function saleProductAction(Request $request)
    {

        $em    = $this->getDoctrine()->getManager();
        $dql   = "SELECT g FROM CatalogBundle:Goods g WHERE g.active = true AND g.sale = true";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            12/*limit per page*/
        );
        return $this->render('@Catalog/Goods/saleProduct.html.twig', ['goods' => $pagination]);
    }

    /**
     * @Route("/searchProduct/", name="searchProduct", options = { "expose" = true })
     * @param Request $request
     * @return Response
     */
    public function searchProductsAction(Request $request)
    {
        $em    = $this->getDoctrine()->getManager();
        $search = $request->get('search');
        $dql   = "SELECT g FROM CatalogBundle:Goods g WHERE g.active = true and g.name LIKE '%$search%'";

        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            12/*limit per page*/
        );
        return $this->render('@Catalog/Goods/searchProduct.html.twig', ['goods' => $pagination]);
    }

    /**
     * @param $id
     * @param $page
     * @param Request $request
     * @return Response
     */
    public function getGoodsByCategoryIdAction($id, $page, $request)
    {
        $em    = $this->getDoctrine()->getManager();
        $dql   = "SELECT g FROM CatalogBundle:Goods g WHERE g.active = TRUE and g.category = $id ORDER BY g.id DESC";
        $minValue = $request->get('minValue');
        $maxValue = $request->get('maxValue');

        if($minValue || $maxValue)
        {
            $dql = "SELECT g FROM CatalogBundle:Goods g WHERE g.active = TRUE and g.category = $id and g.price BETWEEN $minValue and $maxValue ORDER BY g.id DESC";
        }
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $page/*page number*/,
            9/*limit per page*/
        );

        return $this->render('@Catalog/Goods/getGoodsByCategoryId.html.twig', ['goods' => $pagination]);
    }

    /**
     * @Route("/usersGoods", name="usersGoods", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function usersGoodsAction(Request $request)
    {
        if(!$this->getUser())
            throw $this->createNotFoundException();
        $id = $this->getUser()->getId();
        $em    = $this->getDoctrine()->getManager();
        $dql   = "SELECT g FROM CatalogBundle:Goods g WHERE g.user = $id ORDER BY g.id DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1) /*page number*/,
            20 /*limit per page*/
        );

        return $this->render('@Catalog/Goods/usersGoods.html.twig', ['goods' => $pagination]);
    }

    /**
     * @Route("/usersGoods/edit/{id}", name="editProduct", options = { "expose" = true })
     * @param Request $request
     * @param $id
     * @return JsonResponse|RedirectResponse|Response
     */
    public function editProductAction(Request $request, $id)
    {
        if($this->getUser())
        {
            $product = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findUsersGoods($this->getUser(), $id);
            if(!$product)
                throw $this->createNotFoundException();
            $form = $this->createForm(new GoodsTypeTwo(), $product);
            if($request->isMethod('POST')) {
                $em = $this->getDoctrine()->getManager();
                $form = $this->createForm(new GoodsTypeTwo(), $product);
                $form->handleRequest($request);
                if ($form->isValid()) {
                    /** @var Goods $goods */
                    $goods = $form->getData();
                    $em->persist($goods);
                    $em->flush();
                    return $this->redirect($this->generateUrl('usersGoods'));
                }
            }
            return $this->render('@Catalog/Goods/editProduct.html.twig', array('form' => $form->createView(), 'id' => $id));
        }
        return new JsonResponse(array('status' => false));
    }

    /**
     * @Route("/deleteProduct/", name="deleteProduct", options = { "expose" = true })
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function deleteProductAction(Request $request)
    {
        if($this->getUser())
        {
            $em = $this->getDoctrine()->getManager();
            $product = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->find($request->get('productId'));
            $em->remove($product);
            $em->flush();
            return $this->redirectToRoute('usersGoods');
        }
        return new JsonResponse(array('status' => false));
    }

    public function getGoodsByManufactureIdAction($id)
    {
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findBy(array('active' => 1, 'category' => $id));

        return $this->render('@Catalog/Goods/index.html.twig', ['goods' => $goods]);
    }

    public function getBestSaleGoodsAction()
    {
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findBy(['active' => 1, 'sale' => 1], ['id' => 'DESC'], 5);
        return $this->render('@Catalog/Goods/getBestSaleGoods.html.twig', ['goods' => $goods]);
    }

    public function sameGoodsAction(Goods $product)
    {
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->getSameGoods($product);

        if(count($goods) <= 5) {
            $goods2 = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->getSameGoodsWithoutCategory($product, 8 - count($goods));
            $goods = array_merge($goods, $goods2);
        }


        return $this->render('CatalogBundle:Goods:sameGoods.html.twig', ['goods' => $goods]);
    }

    public function popularGoodsAction()
    {
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->getPopularsGoods();
        return $this->render('@Catalog/Goods/getPopularsGoods.html.twig', ['goods' => $goods]);
    }

}
