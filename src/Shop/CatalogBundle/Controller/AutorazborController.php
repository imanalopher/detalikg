<?php

namespace Shop\CatalogBundle\Controller;

use Application\Sonata\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/autorazbor")
 */
class AutorazborController extends Controller
{
    /**
     * @Route("", name="autorazbor_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $autoservices = 'Авторазбор, Автомагазины';
        $description = 'Лучшие авторазборы и автомагазины для вашей машины в Бишкеке| Список авторазборы';
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($autoservices . ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('name', 'keywords', $autoservices. ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('property', 'og:site_name', 'Detali.kg')
            ->addMeta('property', 'og:image', 'http://detali.kg/public/images/logo1.png')
            ->addMeta('property', 'og:title', $autoservices)
            ->addMeta('property', 'og:type', 'product')
            ->addMeta('property', 'og:url', $request->getUri())
            ->addMeta('name', 'description', $description . ' ' . 'в Бишкеке Detali.kg')
        ;
        $razbors = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->findBy(array('razbor' => 1));

        return $this->render('CatalogBundle:Autoservice:razbor.html.twig', array('razbors' => $razbors));
    }

    /**
     * @Route("/{id}", name="show_autorazbor", requirements ={"id"="\d+"})
     * @Method("GET")
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function showAction($id, Request $request)
    {
        $autorazbor = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->findOneBy(array('razbor' => 1, 'id' => $id));

        if (!$autorazbor instanceof User)
            throw $this->createNotFoundException('Page not found 404');

        $autoservices = 'Авторазбор, Автомагазины';
        $description = 'Лучшие авторазборы и автомагазины для вашей машины в Бишкеке| Список авторазборы';
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($autoservices . ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('name', 'keywords', $autoservices. ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('property', 'og:site_name', 'Detali.kg')
            ->addMeta('property', 'og:image', 'http://detali.kg/public/images/logo1.png')
            ->addMeta('property', 'og:title', $autoservices)
            ->addMeta('property', 'og:type', 'product')
            ->addMeta('property', 'og:url', $request->getUri())
            ->addMeta('name', 'description', $description . ' ' . 'в Бишкеке Detali.kg')
        ;
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findBy(array('user' => $id));

        return $this->render('CatalogBundle:Store:show.html.twig', array('user' => $autorazbor, 'goods' => $goods));
    }
}
