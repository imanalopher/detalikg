<?php

namespace Shop\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class BrandController extends Controller
{
    public function brandAction()
    {
        $brands = $this->getDoctrine()->getRepository('CatalogBundle:Manufacture')->findBy(array('active' => 1));
        return $this->render('CatalogBundle:Brand:brand.html.twig', array('brands' => $brands));
    }

    /**
     * @Route("/brand/{id}", name="brandInfo", requirements={"id" = "\d+"})
     * @param integer $id
     * @return Response
     */
    public function brandInfoAction($id)
    {
        $brand = $this->getDoctrine()->getRepository('CatalogBundle:Manufacture')->findBy(array('active' => 1, 'id' => $id));
        return $this->render('@Catalog/Brand/infoBrand.html.twig', ['brand' => $brand, 'id' => $id]);
    }
}
