<?php

namespace Shop\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/autoservice")
 */
class AutoserviceController extends Controller
{
    /**
     * @Route(name="service_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $autoservices = 'Автосервисы, Автомагазины';
        $description = 'Лучшие автосервисы и автомагазины для вашей машины';
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($autoservices . ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('name', 'keywords', $autoservices. ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('property', 'og:title', $autoservices)
            ->addMeta('property', 'og:type', 'product')
            ->addMeta('property', 'og:url', $request->getUri())
            ->addMeta('name', 'description', $description . ' ' . 'в Бишкеке Detali.kg')
        ;
        $stores = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->findBy(array('isService' => 1));
        return $this->render('CatalogBundle:Autoservice:index.html.twig', array('stores' => $stores));
    }

    /**
     * @Route("/{id}", name="show_service", requirements ={"id"="\d+"})
     * @Method("GET")
     * @param integer $id
     * @param Request $request
     * @return Response
     */
    public function showAction($id, Request $request)
    {
        $service = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->findOneBy(array('isService' => 1, 'id' => $id));
        $autoservices = 'Автосервисы, Автомагазины Японские и Германские автозапчасти в Бишкеке для вашей машины.';
        $description = $service->getDescription() .' Лучшие автосервисы и автомагазины для вашей машины в Бишкеке. Японские и Германские запчасти в Бишкеке';
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($service->getDescription() . ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('name', 'keywords', $service->getDescription() . ' ' . $autoservices. ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('property', 'og:title', $service->getDescription() .' '.$autoservices)
            ->addMeta('property', 'og:type', 'product')
            ->addMeta('property', 'og:url', $request->getUri())
            ->addMeta('name', 'description', $description . ' ' . 'в Бишкеке Detali.kg')
        ;
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findBy(array('user' => $id));
        return $this->render('CatalogBundle:Store:show.html.twig', array('goods' => $goods, 'user' => $service));
    }
}
