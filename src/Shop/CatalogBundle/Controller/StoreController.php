<?php

namespace Shop\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/store")
 */
class StoreController extends Controller
{
    /**
     * @Route(name="store_index")
     * @Method("GET")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $autoservices = 'Список магазинов автозапчастей для вашей машины в Бишкеке.';
        $description = 'Список магазинов автозапчастей для вашей машины в Бишкеке. Купите Б/У запчасти в Бишкеке из Японии и Германии по низким ценам.';
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($autoservices . ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('name', 'keywords', $autoservices. ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('property', 'og:title', $autoservices)
            ->addMeta('property', 'og:type', 'product')
            ->addMeta('property', 'og:url', $request->getUri())
            ->addMeta('name', 'description', $description . ' ' . 'в Бишкеке Detali.kg')
        ;
        $stores = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->findBy(array('isStore' => 1));
        return $this->render('CatalogBundle:Store:index.html.twig', array('stores' => $stores));
    }

    /**
     * Finds and displays a News entity.
     *
     * @Route("/{id}", name="store_show", requirements={"id" = "\d+"})
     * @Method("GET")
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findBy(array('user' => $id));
        $user = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->find($id);
        return $this->render('CatalogBundle:Store:show.html.twig', ['goods' => $goods, 'user' => $user]);
    }
}
