<?php

namespace Shop\CatalogBundle\Repository;
use Doctrine\ORM\EntityRepository;

/**
 * CommentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CommentRepository extends EntityRepository
{
    public function getAVGRating($id)
    {
        $qb = $this->createQueryBuilder('c');
        $res = $qb
            ->select('AVG(c.rating)')
            ->where('c.product=:productId')
            ->setParameter('productId', $id)
            ->getQuery();

        return $res->getOneOrNullResult();
    }
}
