<?php

namespace Shop\CatalogBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Shop\CatalogBundle\Entity\Goods;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class GoodsAdmin extends AbstractAdmin
{
    protected $translationDomain = 'SonataPageBundle';
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id'
    );
    /**
     * @var Goods $product
     */
    public function prePersist($product)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $product->setUser($user);
    }

    protected function configureShowField(ShowMapper $showmapper)
    {
        $showmapper
            ->add('id', null, array('label' => 'ID'))
            ->add('name', null, array('label' => 'Название'))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('fullDescription', null, array('label' => 'Полное описание'))
            ->add('price', null, array('label' => 'Цена'))
            ->add('imagePath', 'sonata_media_type', array('label' => 'Галерея', 'provider' => 'sonata.media.provider.image', 'context' => 'default'))
            ->add('active', null, array('label' => 'Активен'))
            ->add('amount', null, array('label' => 'Количество'))
            ->add('lastUpdate', 'date', array('label' => 'Последнее обновление'))
            ->add('views', null, array('label' => 'Количество просмотров'))
            ->add('characteristic', null, array('label' => 'Характеристика'));

    }

    protected function configureFormFields(FormMapper $formmapper)
    {
        $formmapper
            ->with('General')
            ->add('name', null, array('label' => 'Название'))
            ->add('price', null, array('label' => 'Цена'))
            ->add('imagePath', 'sonata_type_model_list', array('required' => false, 'label' => 'Галерея'), array('link_parameters' => array('context' => 'default')))
            ->add('active', null, array('label' => 'Активен'))
            ->add('hot', null, array('label' => 'Новый'))
            ->add('sale', null, array('label' => 'Скидка'))
            ->add('amount', null, array('label' => 'Количество'))
            ->add('lastUpdate', 'date', array('label' => 'Последнее обновление'))
            ->add('characteristic', 'text', array('label' => 'Характеристика', 'required' => false))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('fullDescription', 'textarea', array('label' => 'Полное описание'))
            ->add('review', CKEditorType::class,
                array('label' => 'Обзор', 'config' => array('width' => '1300px', 'resize_enabled' => true, 'resize_minHeight' => '745px', 'resize_minWidth' => '700px', 'resize_maxWidth' => '700px'), 'config_name' => 'my_config')
            )
            ->add('category', 'sonata_type_model', array(
                'multiple' => false,
                'expanded' => true,
                'by_reference' => true
            ))
            ->add('brand', 'sonata_type_model', array(
                'multiple' => false,
                'expanded' => true,
                'by_reference' => true
            ))
            ->end();
    }

    protected function configureListFields(ListMapper $listmapper)
    {
        $listmapper
            ->addIdentifier('name', null, array('label' => 'ID'))
            ->addIdentifier('user', null, array('label' => 'Хозяин'))
            ->add('price', null, array('editable' => true, 'label' => 'Цена'))
            ->add('active', 'boolean', array('editable' => true, 'label' => 'Активен'))
            ->add('amount', 'integer', array('label' => 'Количество', 'template' => 'ShopCatalogBundle:Admin:edit_integer.html.twig'))
            ->add('category', null, array('label' => 'Категорий'))
            ->add('image', null, array('label'=>'Фото','template' => 'CatalogBundle:Admin:goods_list_image.html.twig'))
            ->add('imagePath', 'sonata_type_model_list', array('editable' => true, 'label' => 'Галерея'));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array('label' => 'Название'))
            ->add('user', null, array('label' => 'Хозяин'))
            ->add('price', null, array('label' => 'Цена'))
            ->add('active', null, array('label' => 'Активен'))
            ->add('last_update', 'doctrine_orm_date_range', array('label' => 'Последнее обновление'))
            ->add('imagePath', null, array('label' => 'Галерея'), null, array('expanded' => false, 'empty_value' => ""));
    }

}