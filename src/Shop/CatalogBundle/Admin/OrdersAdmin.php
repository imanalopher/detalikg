<?php

namespace Shop\CatalogBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OrdersAdmin extends AbstractAdmin
{

    protected function configureShowField(ShowMapper $showmapper)
    {
        $showmapper
            ->add('id', null, array('label' => 'ID'))
            ->add('phone', null, array('label' => 'Название'));

    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('email', null, array('label' => 'E-mail'))
            ->add('created', null, array('label' => 'Созданно'))
            ->add('status', null, array('label' => 'Статус'))
            ->add('address', 'textarea', array('label' => 'Адрес'))
            ->add('phone', null, array('label' => 'Тел.'));
    }

    protected function configureListFields(ListMapper $listmapper)
    {
        $listmapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->add('email', null, array('label' => 'E-mail'))
            ->add('created', null, array('label' => 'Созданно'))
            ->add('status', null, array('editable' => true, 'label' => 'Статус'))
            ->add('address', null, array('label' => 'Адрес'))
            ->add('phone', null, array('label' => 'Тел.'));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('email', null, array('label' => 'Категорий'))
            ->add('created', null, array('label' => 'orderProducts'))
            ->add('phone', null, array('label' => 'Бренд'));
    }

}