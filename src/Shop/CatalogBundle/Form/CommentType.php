<?php

namespace Shop\CatalogBundle\Form;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class CommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', 'textarea', array(
                'attr' => array('class' => 'form-control', 'placeholder' => 'Комментарий')
            ))
            ->add('author', TextType::class, array(
                'label' => 'Автор',
                'attr' => array('class' => 'form-control')
            ))
            ->add('rating',  'choice', array(
                    'choices' => array(1, 2, 3, 4, 5),
                    'choices_as_values' => true,
                    'choice_label' => null,
                    'expanded' => true,
                    'multiple' => false,
                    'by_reference' => true,
                    'data' => 4
                )
            )
            ->add('email', EmailType::class, array(
                'label' => 'E-mail',
                'required' => true,
                'attr' => array('class' => 'form-control')
            ))
            ->add('captcha', CaptchaType::class, array(
                'label' => false,
                'width' => 121,
                'height' => 60,
                'length' => 4,
                'reload' => true,
                'as_url' => true,
                'invalid_message' => "Вы ввели неправильный код с картинки (CAPTCHA)",
                'label_attr' => array('class' => 'form-control')
            ))
            ->add('submit', 'submit', array(
                'label' => 'Оставить комментарий',
                'attr' => array('class' => 'btn btn-primary')
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shop\CatalogBundle\Entity\Comment'
        ));
    }
}
