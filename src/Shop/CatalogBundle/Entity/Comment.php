<?php

namespace Shop\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="Shop\CatalogBundle\Repository\CommentRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Goods
     * @ORM\ManyToOne(targetEntity="Goods", inversedBy="comments")
     * @ORM\JoinColumn(onDelete="cascade")
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text")
     * @Assert\Length(
     *     min = 20,
     *     max = 9999,
     *     minMessage = "Поле КОММЕНТАРИИ слишком короткое (Минимум: {{ limit }} симв.).",
     *     maxMessage = "Поле КОММЕНТАРИИ слишком длинное (Макс: {{ limit }} симв.)."
     * )
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetimetz")
     */
    private $created;

    /**
     * @var int
     *
     * @ORM\Column(name="rating", type="integer")
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", nullable=false)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=false)
     *
     * @Assert\Email(
     *     message = "Email '{{ value }}' в неправильном формате.",
     *     checkMX = true
     * )
     */
    private $email;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * Get rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @return Goods
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Goods $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->created = new \DateTime();
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}

