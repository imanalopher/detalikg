<?php

namespace Shop\CoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}", name="hello")
     * @param $name
     * @return Response
     */
    public function indexAction($name)
    {
        return $this->render('CoreBundle:Default:index.html.twig', ['name' => $name]);
    }

    /**
     * @Route("/sitemap.{_format}", name="sitemap", Requirements={"_format" = "xml"})
     * @Template("@Core/Default/sitemap.xml.twig")
     * @return array
     */
    public function siteMapAction(Request $request)
    {
        $cars = Yaml::parse(file_get_contents(__DIR__.'/../Resources/config/marka.yml'));
        $em = $this->getDoctrine()->getManager();
        $urls = array();
        $hostname = $request->getHost();
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findBy(array('active' => true));
        $urls[] = array('loc' => $this->get('router')->generate('homePage'),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24), 'changefreq' => 'daily', 'priority' => '1.0');
        $urls[] = array('loc' => $this->get('router')->generate('aboutPage'),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24), 'changefreq' => 'daily', 'priority' => '0.9');
        $urls[] = array('loc' => $this->get('router')->generate('feedback'),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24), 'changefreq' => 'daily', 'priority' => '0.8');

        foreach ($em->getRepository('CatalogBundle:Category')->findAll() as $product) {
            $urls[] = array('loc' => $this->get('router')->generate('getCategory',
                array('id' => $product->getId())),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24),'changefreq' => 'daily', 'priority' => '0.8');
        }

        foreach ($cars['cars'] as $car) {
            $urls[] = array('loc' => $this->get('router')->generate('searchProduct', array('search' => $car)),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24), 'changefreq' => 'daily', 'priority' => '0.7');
        }

        foreach ($goods as $product) {
            $urls[] = array('loc' => $this->get('router')->generate('goodsGetInfo', array('id' => $product->getId())),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24), 'changefreq' => 'daily', 'priority' => '0.8');
        }

        $urls[] = array('loc' => $this->get('router')->generate('store_index'),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24), 'changefreq' => 'daily', 'priority' => '0.8');

        $stores = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->findBy(array('isStore' => 1));
        $urls[] = array('loc' => $this->get('router')->generate('store_index'),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24), 'changefreq' => 'daily', 'priority' => '0.8');
        foreach ($stores as $store) {
            $urls[] = array('loc' => $this->get('router')->generate('store_show', array('id' => $store->getId())),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24), 'changefreq' => 'daily', 'priority' => '0.7');
        }

        $services = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->findBy(array('isService' => 1));
        $urls[] = array('loc' => $this->get('router')->generate('service_index'),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24), 'changefreq' => 'daily', 'priority' => '0.8');
        foreach ($services as $service)
        {
            $urls[] = array('loc' => $this->get('router')->generate('show_service', array('id' => $service->getId())),'lastmod' => date("Y-m-d", time() - 60 * 60 * 24), 'changefreq' => 'daily', 'priority' => '0.7');
        }

        return array('urls' => $urls, 'hostname' => "http://".$hostname);
    }

    /**
     * @Route("/routes")
     * @return Response
     */
    public function showAllRoutesAction()
    {
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findAll();
        return $this->render('@Core/Default/showAllRoutes.html.twig', array('goods' => $goods));
    }
}
